package edu.uoc.android.restservice.ui.enter;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;

    ProgressDialog progressDialog, progressDialog2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);

        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String loginName = getIntent().getStringExtra("loginName");

        //initProgressBar();

        mostrarDatosBasicos(loginName);
    }

    TextView labelFollowing, labelRepositories, labelFollowers;
    private void initProgressBar()
    {
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);

        labelFollowing = (TextView)findViewById(R.id.labelFollowing);
        labelFollowing.setVisibility(View.INVISIBLE);

        labelRepositories = (TextView) findViewById(R.id.labelRepositories);
        labelRepositories.setVisibility(View.INVISIBLE);

        labelFollowers = (TextView) findViewById(R.id.labelFollowers);
        labelFollowers.setVisibility(View.INVISIBLE);

    }

    private void mostrarDatosBasicos(String loginName){
        // Instancia para mostrar datos del usuario
        progressDialog = new ProgressDialog(this);
        // Buscamos los usuarios
        progressDialog.setMessage("Buscando usuario");
        // Se muestra un cuadro de dialogo
        progressDialog.show();

        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                // Finaliza el dialogo de progreso
                progressDialog.dismiss();
                Owner owner = response.body();
                if (owner != null) { // Realizamos una condicion para verificar datos erroneos
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    // Se envia el numero de personas que se sigue dicho usuario
                    textViewFollowing.setText(owner.getFollowing().toString());
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);

                }else {
                    Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                // Muestra un mensaje de error por falla de verificacion
                Toast.makeText(InfoUserActivity.this, "Error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

        // Aparecen datos mientras busca los seguidores
        progressDialog2 = new ProgressDialog(this);
        // Se crea una instancia para la busqueda
        progressDialog2.setMessage("Buscando seguidores");
        // Inicia
        progressDialog2.show();

        // Se hace una llamada a la lista de seguidores
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);

        callFollowers.enqueue(new Callback<List<Followers>>() {
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                progressDialog2.cancel();
                // Se guarda el resultado obtenido
                List<Followers> list = response.body();
                if (list != null) {
                    // Se envía la lista para mostrar los datos
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list);
                    // Se guarda los datos en el adaptador
                    recyclerViewFollowers.setAdapter(adapter);
                } else {
                    // Si no hay informacion, muestra mensaje de error
                    Toast.makeText(InfoUserActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                // Muestra mensaje de error si ocurre alguna falla
                Toast.makeText(InfoUserActivity.this, "Se ha producido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
