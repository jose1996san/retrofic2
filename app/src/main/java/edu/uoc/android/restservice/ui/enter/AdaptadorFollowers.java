package edu.uoc.android.restservice.ui.enter;

import android.app.Application;
import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;



public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    List<Followers> listaFollowers;
    Context context;

    // Creamos el metodo que asigna los datos obtenidos
    public AdaptadorFollowers(List<Followers> listaFollowers) {
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        context = parent.getContext();
        // Obtenemos el contexto de la aplicación y lo retorna un resultado
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        // Se envia el nombre del seguidor y enviamos la imagen del seguidor
        holder.etiNombre.setText(listaFollowers.get(position).getLogin());
        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.imagen);
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {
        // Se definen las variables que se van a utilizar

        TextView etiNombre;
        ImageView imagen;

        public ViewHolderFollowers(View itemView) {
            super(itemView);
            // Se crea el metodo que almacena la informacion
            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);
            imagen = (ImageView) itemView.findViewById(R.id.imageViewLista);
        }
    }
}
